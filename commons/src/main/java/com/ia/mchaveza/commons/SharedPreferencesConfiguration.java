package com.ia.mchaveza.commons;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by knock on 05/05/2017.
 */

public class SharedPreferencesConfiguration {

    private Context mContext;
    private static SharedPreferences sharedPreferences;

    public SharedPreferencesConfiguration(Context context) {
        mContext = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public String getStringPreference(String stringPreference) {
        return sharedPreferences.getString(stringPreference, null);
    }

    public String getStringPreference(String stringPreference, String defaultValue) {
        return sharedPreferences.getString(stringPreference, defaultValue);
    }

    public Boolean getBooleanPreference(String stringPreference, Boolean defaultValue) {
        return sharedPreferences.getBoolean(stringPreference, defaultValue);
    }

    public int getIntPreference(String stringPreference) {
        return sharedPreferences.getInt(stringPreference, -1);
    }

    public void setStringPreference(String preferenceName, String preference) {
        sharedPreferences.edit().putString(preferenceName, preference).apply();
    }

    public void setBooleanPreference(String preferenceName, Boolean preference) {
        sharedPreferences.edit().putBoolean(preferenceName, preference).apply();
    }

    public void setIntPreference(String preferenceName, int preference) {
        sharedPreferences.edit().putInt(preferenceName, preference).apply();
    }

    public void clearPreference(String preferenceName){
        sharedPreferences.edit().remove(preferenceName).apply();
    }

}
